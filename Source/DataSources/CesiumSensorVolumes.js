import initialize from './initialize';
import ConicSensorGraphics from './conic/ConicSensorGraphics';
import ConicSensorVisualizer from './conic/ConicSensorVisualizer';
import CustomPatternSensorGraphics from './custom/CustomPatternSensorGraphics';
import CustomPatternSensorVisualizer from './custom/CustomPatternSensorVisualizer';
import CustomSensorVolume from './custom/CustomSensorVolume';
import RectangularPyramidSensorVolume from './rectangular/RectangularPyramidSensorVolume';
import RectangularSensorGraphics from './rectangular/RectangularSensorGraphics';
import RectangularSensorVisualizer from './rectangular/RectangularSensorVisualizer';

initialize();

export default {
	ConicSensorGraphics,
	ConicSensorVisualizer,
	CustomPatternSensorGraphics,
	CustomPatternSensorVisualizer,
	CustomSensorVolume,
	RectangularPyramidSensorVolume,
	RectangularSensorGraphics,
	RectangularSensorVisualizer
};
